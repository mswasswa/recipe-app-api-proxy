# italemwa - Recepie App API Proxy

NGINX proxy app for our recipe API

itale hint: API is the acronym for Application Programming Interface, which is a software intermediary that allows two applications to talk to each other. Each time you use an app like Facebook, send an instant message, or check the weather on your phone, you're using an API.

## Usage  (How to use the project)

### Environment Variables  (environemnt variables that we use to configure our NGINX proxy)

* `LISTEN_PORT` - Port to listen on (default: `8000`) 
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000` )