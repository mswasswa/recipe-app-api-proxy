# this is our nginx configuration file template
# called .tpl becauss this is a template that will be passed through somethoing called m-substitute
# to poputae values based on environment variables 

# "server" -  blog, default blog all nginx configurations need to have
# "listen" -  tell lnginx which port to listen to this port is decided by the environment variable LISTEN_PORT 
# "location /static" - this is the location in Django where we catch our static files
# "location /" -  catch the rest of the requests that dont match static 
# "uwsgi_pass" - passes requests through to uwsgi service that runs our python application, 
#                specify hostname and port that we want to forward requests to, this will also be an environment variable 
#                so all requests that match the location to our uwsgi service
#                when you pass things to a uwsgi service you need to specify some default parameters, these are called uwsgi parameters "https://uwsgi-docs.readthedocs.io/en/latest/Nginx.html#what-is-the-uwsgi-params-file"

#                 

server {
  listen ${LISTEN_PORT};

  location /static {
    alias /vol/static;
  }

  location / {
    uwsgi_pass             ${APP_HOST}:${APP_PORT};
    include                /etc/nginx/uwsgi_params;
    client_max_body_size   10M;
  }
}
